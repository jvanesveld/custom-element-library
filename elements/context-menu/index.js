import { html, CustomElement, renderShadow } from "../../utils/index.js";

class ContextMenu extends CustomElement {
  static get observedAttributes() {
    return ["x-pos", "y-pos"];
  }

  show = false;
  x_pos = 0;
  y_pos = 0;
  options = [];

  constructor() {
    super();
    this.initInput("show");
    this.initInput("x_pos", { sync: true, attrName: "x-pos", initCheck: true });
    this.initInput("y_pos", { sync: true, attrName: "y-pos", initCheck: true });
    this.initInput("options");
    this.listenCloseEvents();
  }

  listenCloseEvents() {
    window.addEventListener("blur", () => {
      this.show = false;
    });
    document.addEventListener("mousedown", () => {
      this.show = false;
    });
    document.addEventListener("mousewheel", () => {
      this.show = false;
    });
    document.addEventListener("keydown", (e) => {
      if (e.key === "Escape") {
        this.show = false;
      }
    });
    this.addEventListener("mousedown", (e) => {
      e.stopPropagation();
    });
  }

  inputChanged() {
    if (this.show && this.options && this.options.length > 0) {
      renderShadow(
        this,
        html`
          <style>
            .container {
              position: absolute;
              left: ${this.x_pos};
              top: ${this.y_pos};
              color: white;
              z-index: 1000;
            }
            .menu {
              position: relative;
              display: grid;
              grid-auto-flow: row;
              padding-top: 0.5em;
              padding-bottom: 0.5em;
              min-width: 10em;
              background: var(--bg-dark, #333333);
            }
            .menu::before {
              position: absolute;
              background: var(--bg-dark, #333333);
              content: "";
              right: 0;
              left: 0;
              top: 0;
              bottom: 0;
              filter: drop-shadow(0 0 0.15em black);
            }
            .option:hover {
              background: var(--bg-light, #555555);
              cursor: pointer;
            }
            .sub-options {
              display: none;
              position: absolute;
              left: 100%;
              top: -0.5rem;
            }
            .option:hover > .sub-options {
              display: block;
            }
            .option {
              display: grid;
              grid-template-columns: 1fr auto;
              align-items: center;
              position: relative;
              padding-left: 1em;
              padding-right: 1em;
              padding-top: 0.2em;
              padding-bottom: 0.2em;
            }
            .separator {
              position: relative;
              margin: 8px;
            }
            .separator:after {
              position: absolute;
              content: "";
              left: 0;
              right: 0;
              top: 50%;
              border-bottom: solid 1px rgb(256, 256, 256, 0.5);
            }
            fa-icon {
              font-size: 0.8rem;
            }
          </style>
          <div class="container">
            <div class="menu">
              ${this.options.map((option) => this.renderOption(option))}
            </div>
          </div>
        `
      );
    } else {
      renderShadow(this, html``);
    }
  }

  renderOption(option) {
    if (option.separator) {
      return html`<span class="separator"></span>`;
    } else if (option.options) {
      return html` <span class="option">
        ${option.name}
        <fa-icon icon="fas fa-chevron-right"></fa-icon>
        <div class="menu sub-options">
          ${option.options.map((option) => this.renderOption(option))}
        </div>
      </span>`;
    } else {
      return html` <span
        class="option"
        @click=${(e) => {
          e.stopPropagation();
          option.action();
          this.show = false;
        }}
      >
        ${option.name}
      </span>`;
    }
  }
}

customElements.define("context-menu", ContextMenu);
