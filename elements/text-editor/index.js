import { renderShadow, html, CustomElement } from "../../utils/index.js";
import "./codemirror/lib/codemirror.js";

const dir = import.meta.url.substring(0, import.meta.url.lastIndexOf("/"));

const intersectObserver = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (
      entry.isIntersecting &&
      entry.target.tagName == "TEXT-EDITOR" &&
      entry.target.cm
    ) {
      setTimeout(() => entry.target.cm.refresh(), 1); // idk chrome is weird :/
    }
  });
});

class TextEditor extends CustomElement {
  static get observedAttributes() {
    return ["mode", "theme", "content"];
  }

  content = "test";
  mode = "javascript";
  theme = "dracula";
  dependencies = {
    htmlmixed: ["javascript", "css", "xml"],
  };

  constructor() {
    super();
    this.render();
    this.constructCodeMirror();
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          #cm-container {
            width: 100%;
            height: 100%;
          }
          .CodeMirror {
            font-size: 16px;
            height: 100% !important;
          }
        </style>
        <link rel="stylesheet" href="${dir}/codemirror/lib/codemirror.css" />
        ${this.theme != "default"
          ? html`<link
              rel="stylesheet"
              href="${dir}/codemirror/theme/${this.theme}.css"
            />`
          : ""}
        <div id="cm-container"></div>
      `
    );
  }

  async importMode(mode) {
    return import(`./codemirror/mode/${mode}/${mode}.js`);
  }

  async updateMode(mode) {
    if (mode) {
      await (dependencies[mode] || []).forEach(async (dep) =>
        this.importMode(dep)
      );
      await this.importMode(mode);
      this.cm.setOption("mode", mode);
    }
  }

  async updateTheme(theme) {
    if (theme) {
      this.render();
      this.cm.setOption("theme", theme);
    }
  }

  async updateContent(content) {
    if (content && this.cm.getValue() != content) {
      this.render();
      this.cm.setValue(content);
    }
  }

  async constructCodeMirror() {
    await this.importMode(this.mode);
    this.cm = CodeMirror(this.shadowRoot.querySelector("#cm-container"), {
      lineNumbers: true,
      gutter: true,
      value: "",
      mode: this.mode,
      theme: this.theme,
    });
    intersectObserver.observe(this);
    this.cm.on("changes", (cm, changeList) => {
      this.content = cm.getValue();
    });
    this.initInput("mode", { sync: true, initCheck: true }, () =>
      this.updateMode(this.mode)
    );
    this.initInput("theme", { sync: true, initCheck: true }, () =>
      this.updateTheme(this.theme)
    );
    this.initInput("content", { sync: true, initCheck: true }, () =>
      this.updateContent(this.content)
    );
  }
}

customElements.define("text-editor", TextEditor);
