import { CustomElement, renderShadow, html } from "../../utils/index.js";
import { CSS3DRenderer, CSS3DObject } from "./three/renderers/CSS3DRenderer.js";
import { Scene } from "./three/three.module.js";

class ThreeCSS extends CustomElement {
  static get observedAttributes() {
    return ["z-index"];
  }

  camera;
  renderer;
  scene;
  disconnect;
  objElement;
  object;
  x = 0;
  y = 0;
  z = 0;
  ry = 0;
  rz = 0;
  rx = 0;
  zIndex = 0;

  constructor() {
    super();
    this.initInput("x");
    this.initInput("y");
    this.initInput("z");
    this.initInput("rx");
    this.initInput("ry");
    this.initInput("rz");
    this.initInput("zIndex", { sync: true, attrName: "z-index" });
    this.initInput("disconnect", {}, (oldDisconnect, _) => oldDisconnect?.());
    this.render();
    this.renderer = new CSS3DRenderer();
    this.scene = new Scene();
    this.object = new CSS3DObject(this.objElement);
    this.scene.add(this.object);
    this.container.append(this.renderer.domElement);
  }

  inputChanged() {
    this.object.position.set(this.x, this.y, this.z);
    this.object.rotation.set(this.rx, this.ry, this.rz);
  }

  connectedCallback() {
    const three = this.closest("three-js");
    if (three) {
      this.camera = three.camera;
      this.updateSize(three.clientWidth, three.clientHeight);
      const resize_disconnect = three.onResize((width, height) =>
        this.updateSize(width, height)
      );
      const animation_disconnect = three.onAnimation((t) => this.animation(t));
      this.disconnect = () => {
        resize_disconnect();
        animation_disconnect();
      };
    }
  }

  updateSize(width, height) {
    this.renderer.setSize(width, height);
    this.renderer.render(this.scene, this.camera);
  }

  animation() {
    if (this.camera) {
      this.renderer.render(this.scene, this.camera);
    }
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          #container {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: ${this.zIndex};
          }
        </style>
        <div id="object">
          <slot></slot>
        </div>
        <div id="container"></div>
      `
    );
    this.objElement = this.shadowRoot.querySelector("#object");
    this.container = this.shadowRoot.querySelector("#container");
  }
}

customElements.define("three-css", ThreeCSS);
