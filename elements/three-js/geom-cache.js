import { GLTFLoader } from "./three/loaders/GLTFLoader.js";
import { asyncCallback } from "../../utils/async-callback.js";

const cache = {};
const loader = new GLTFLoader();

export async function getGeom(url) {
  if (!cache[url]) {
    cache[url] = asyncCallback((callback) => loader.load(url, callback));
  }
  let loaded = await cache[url];
  return loaded.scene.children[0].geometry.clone();
}
