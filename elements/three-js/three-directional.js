import { CustomElement } from "../../utils/index.js";
import { DirectionalLight, Object3D } from "./three/three.module.js";

class ThreeDirectional extends CustomElement {
  static get observedAttributes() {
    return ["color", "cast-shadow"];
  }

  cast_shadow = false;
  camera;
  scene;
  disconnect;
  cast_shadow = false;
  intensity = 0.7;
  map_size = 1000;
  color = 0xdddddd;
  position = { x: 0, y: 0, z: 1 };
  target = { x: 0, y: 0, z: 0 };
  light;

  constructor() {
    super();
    this.initInput("map_size");
    this.initInput("intensity");
    this.initInput("color", { sync: true });
    this.initInput("cast_shadow", {
      sync: true,
      attrName: "cast-shadow",
      isBool: true,
    });
    this.initInput("position");
    this.initInput("target");
    this.initInput("cast_shadow", {
      sync: true,
      attrName: "cast-shadow",
      isBool: true,
      initCheck: true,
    });
    this.light = new DirectionalLight(this.color, this.intensity);
    this.light.target = new Object3D();
    this.inputChanged();
  }

  inputChanged() {
    this.light.color.set(this.color);
    this.light.position.set(this.position.x, this.position.y, this.position.z);
    this.light.castShadow = this.cast_shadow;
    this.light.target.position.set(this.target.x, this.target.y, this.target.z);
  }

  connectedCallback() {
    const three = this.closest("three-js");
    this.scene = three.scene;
    this.camera = three.camera;
    this.disconnect?.(); // Disconnect from previous three-js container
    this.scene.add(this.light);
    this.scene.add(this.light.target);
    this.disconnect = three.onAnimation((t) => {
      this.animation(t);
    });
  }

  animation(t) {
    this.light.shadow.mapSize.width = 1024; // default
    this.light.shadow.mapSize.height = 1024; // default
    this.light.shadow.camera.near = 0.5;
    this.light.shadow.camera.far = this.map_size * 2;
    this.light.shadow.camera.top = this.map_size;
    this.light.shadow.camera.bottom = -this.map_size;
    this.light.shadow.camera.left = -this.map_size;
    this.light.shadow.camera.right = this.map_size;
    this.light.shadow.camera.updateProjectionMatrix();
  }
}

customElements.define("three-directional", ThreeDirectional);
