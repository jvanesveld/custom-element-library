import { CustomElement } from "../../utils/index.js";
import unique_id from "../../utils/unique_id.js";
import { getGeom } from "./geom-cache.js";
import { Mesh } from "./three/three.module.js";

class ThreeModel extends CustomElement {
  static get observedAttributes() {
    return ["model"];
  }

  scene;
  model;
  material;
  scale = { x: 1, y: 1, z: 1 };
  position = { x: 0, y: 0, z: 0 };
  rotation = { x: 0, y: 0, z: 0 };
  obj;
  obj_id = unique_id();

  constructor() {
    super();
    this.initInput("model", { sync: true, initCheck: true }, () => this.load());
    this.initInput("material");
    this.initInput("scale");
    this.initInput("position");
    this.initInput("rotation");
    this.load();
  }

  connectedCallback() {
    this.scene = this.closest("three-js")?.scene;
    this.load();
  }

  disconnectedCallback() {
    this.unload();
    this.scene = undefined;
  }

  inputChanged() {
    if (this.obj) {
      this.updateProperties();
    }
  }

  async load() {
    if (
      !this.scene ||
      !this.model ||
      this.obj?.name === `${this.obj_id}-${this.model}`
    ) {
      return;
    }
    this.unload();
    const geometry = await getGeom(this.model);
    const material = this.material;
    const mesh = new Mesh(geometry, material);
    this.setModel(mesh);
  }

  unload() {
    if (this.obj) {
      this.scene.remove(this.obj);
    }
  }

  setModel(model) {
    if (this.scene) {
      this.obj = model;
      this.updateProperties();
      this.obj.name = `${this.obj_id}-${this.model}`;
      this.scene.add(this.obj);
    }
  }

  updateProperties() {
    this.obj.castShadow = true;
    this.obj.receiveShadow = true;
    this.obj.material = this.material;
    this.obj.children.forEach((child) => {
      child.material = this.material;
      child.castShadow = true;
      child.receiveShadow = true;
    });
    this.obj.position.set(this.position.x, this.position.y, this.position.z);
    this.obj.rotation.set(this.rotation.x, this.rotation.y, this.rotation.z);
    this.obj.scale.set(this.scale.x, this.scale.y, this.scale.z);
  }
}

customElements.define("three-model", ThreeModel);
