import { CustomElement } from "../../utils/index.js";
import { AmbientLight } from "./three/three.module.js";

class ThreeAmbient extends CustomElement {
  static get observedAttributes() {
    return ["color"];
  }

  scene;
  disconnect;
  intensity = 0.3;
  color = "rgb(255,255,255)";
  light;

  constructor() {
    super();
    this.initInput("intensity");
    this.initInput("color", { sync: true });
    this.initInput("color", { sync: true });
    this.light = new AmbientLight(this.color, this.intensity);
    this.inputChanged();
  }

  inputChanged() {
    this.light.color.set(this.color);
    this.light.intensity = this.intensity;
  }

  connectedCallback() {
    const three = this.closest("three-js");
    this.scene = three.scene;
    this.disconnect?.(); // Disconnect from previous three-js container
    if (this.scene) {
      this.scene.add(this.light);
      this.disconnect = three.onAnimation((t) => {
        this.animation(t);
      });
    }
  }

  animation(t) {}
}

customElements.define("three-ambient", ThreeAmbient);
