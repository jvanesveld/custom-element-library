import { CustomElement } from "../../utils/index.js";
import {
  Mesh,
  MeshPhongMaterial,
  PlaneGeometry,
} from "./three/three.module.js";

class ThreePlane extends CustomElement {
  static get observedAttributes() {
    return ["model"];
  }

  scene;
  material = new MeshPhongMaterial();
  width = 100;
  height = 100;
  position = { x: 0, y: 0, z: 0 };
  rotation = { x: 0, y: 0, z: 0 };
  plane;

  constructor() {
    super();
    this.initInput("width");
    this.initInput("height");
    this.initInput("material");
    this.initInput("position");
    this.initInput("rotation");
  }

  inputChanged() {
    if (this.scene) {
      this.updatePlane();
    }
  }

  updatePlane() {
    if (this.plane) {
      this.scene.remove(this.plane);
    }
    const geometry = new PlaneGeometry(this.width, this.height);
    this.plane = new Mesh(geometry, this.material);
    this.plane.receiveShadow = true;
    this.plane.position.set(this.position.x, this.position.y, this.position.z);
    this.plane.rotation.set(this.rotation.x, this.rotation.y, this.rotation.z);
    this.scene.add(this.plane);
  }

  connectedCallback() {
    this.scene = this.closest("three-js")?.scene;
    this.updatePlane();
  }
}

customElements.define("three-plane", ThreePlane);
