import { CustomElement, renderShadow, html } from "../../utils/index.js";
import {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
} from "./three/three.module.js";
import "./three-model.js";
import "./three-css.js";
import "./three-flatcss.js";
import "./three-directional.js";
import "./three-ambient.js";
import "./three-plane.js";

class ThreeJS extends CustomElement {
  static get observedAttributes() {
    return ["click-through", "z-index"];
  }

  z_index = 0;
  click_through = false;
  camera;
  scene;
  renderer;
  animations = [];
  resizes = [];

  constructor() {
    super();
    this.initInput(
      "click_through",
      { sync: true, attrName: "click-through", isBool: true, initCheck: true },
      () => {
        this.setPointerEvents();
      }
    );
    this.initInput(
      "z_index",
      { sync: true, attrName: "z-index", initCheck: true },
      () => {
        this.container.style.setProperty("z-index", this.z_index);
      }
    );

    this.render();
    this.initThreeJS();

    requestAnimationFrame((time) => this.animation(time));
    const observer = new ResizeObserver((_) => {
      this.resizeCanvasToDisplaySize();
    });
    observer.observe(this.container);
    this.setPointerEvents();
    this.container.style.setProperty("z-index", this.z_index);
  }

  initThreeJS() {
    this.camera = new PerspectiveCamera(70, 2, 1, 1000);
    this.camera.position.z = 1000;
    this.camera.position.y = -750;
    this.camera.rotation.x = 0.5;
    this.scene = new Scene();
    this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.shadowMap.enabled = true;
    this.container.append(this.renderer.domElement);
  }

  setPointerEvents() {
    const pointer_events = this.click_through ? "none" : "auto";
    this.renderer.domElement.style.setProperty(
      "pointer-events",
      pointer_events
    );
  }

  resizeCanvasToDisplaySize() {
    const canvas = this.renderer.domElement;
    const width = this.container.clientWidth;
    const height = this.container.clientHeight;

    if (canvas.width !== width || canvas.height !== height) {
      this.renderer.setSize(width, height, false);
      this.camera.aspect = width / height;
      this.camera.updateProjectionMatrix();
      this.resizes.forEach((resize) => resize(width, height));
    }
  }

  onAnimation(animation) {
    this.animations.push(animation);
    return () => {
      let i = this.animations.indexOf(animation);
      if (i != -1) {
        this.animations.splice(this.animations.indexOf(animation), 1);
        return true;
      }
      return false;
    };
  }

  onResize(resize) {
    this.resizes.push(resize);
    return () => {
      let i = this.resizes.indexOf(resize);
      if (i != -1) {
        this.resizes.splice(this.resizes.indexOf(resize), 1);
        return true;
      }
      return false;
    };
  }

  animation(time) {
    requestAnimationFrame((time) => this.animation(time));
    this.animations.forEach((animation) => animation(time));
    this.renderer.render(this.scene, this.camera);
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          #container {
            position: relative;
            width: 100%;
            height: 100%;
          }
        </style>
        <div id="container">
          <slot></slot>
        </div>
      `
    );
    this.container = this.shadowRoot.querySelector("#container");
  }
}

customElements.define("three-js", ThreeJS);
