import { CustomElement, renderShadow, html } from "../../utils/index.js";
import { CSS2DRenderer, CSS2DObject } from "./three/renderers/CSS2DRenderer.js";
import { Scene } from "./three/three.module.js";

class ThreeFlatCSS extends CustomElement {
  camera;
  disconnect;
  renderer;
  scene;
  disconnect;
  objElement;
  object;
  position = { x: 0, y: 0, z: 0 };
  rotation = { x: 0, y: 0, z: 0 };

  constructor() {
    super();
    this.initInput("position");
    this.initInput("rotation");
    this.initInput("disconnect", {}, (oldDisconnect) => oldDisconnect?.());
    this.render();
    this.renderer = new CSS2DRenderer();
    this.scene = new Scene();
    this.object = new CSS2DObject(this.objElement);
    this.scene.add(this.object);
    this.container.append(this.renderer.domElement);
  }

  inputChanged() {
    this.object.position.set(this.position.x, this.position.y, this.position.z);
    this.object.rotation.set(this.rotation.x, this.rotation.y, this.rotation.z);
  }

  connectedCallback() {
    const three = this.closest("three-js");
    this.camera = three?.camera;
    if (this.camera) {
      this.updateSize(three.clientWidth, three.clientHeight);
      const disconnect_resize = three.onResize((width, height) => {
        this.updateSize(width, height);
      });
      const disconnect_animation = three.onAnimation((t) => {
        this.animation(t);
      });
      this.disconnect = () => {
        disconnect_resize();
        disconnect_animation();
      };
    }
  }

  updateSize(width, height) {
    this.renderer.setSize(width, height, false);
    this.renderer.render(this.scene, this.camera);
  }

  animation(time) {
    if (this.camera) {
      this.renderer.render(this.scene, this.camera);
    }
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          #container {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
          }
        </style>
        <div id="object">
          <slot></slot>
        </div>
        <div id="container"></div>
      `
    );
    this.objElement = this.shadowRoot.querySelector("#object");
    this.container = this.shadowRoot.querySelector("#container");
  }
}

customElements.define("three-flatcss", ThreeFlatCSS);
