import { renderShadow, html, CustomElement } from "../../utils/index.js";

class LayoutSplitV extends CustomElement {
  static get observedAttributes() {
    return ["scale"];
  }

  scale = "0.5";

  constructor() {
    super();
    this.initInput("scale", { sync: true, attrName: "scale" });
  }

  connectedCallback() {
    this.render();
  }

  inputChanged() {
    this.render();
  }

  handleSlotChange() {
    let parent = this.parentElement;
    let slot = this.slot;
    if (!parent) {
      return;
    }
    let top =
      this.top.assignedNodes()[0] === undefined ||
      this.top.assignedNodes()[0].tagName === "LAYOUT-EMPTY";
    let bottom =
      this.bottom.assignedNodes()[0] === undefined ||
      this.bottom.assignedNodes()[0].tagName === "LAYOUT-EMPTY";
    if (top && bottom) {
      this.remove();
      parent.append(document.createElement("layout-empty"));
    } else if (top) {
      this.remove();
      let node = this.bottom.assignedNodes()[0];
      node.slot = slot;
      parent.append(node);
    } else if (bottom) {
      this.remove();
      let node = this.top.assignedNodes()[0];
      node.slot = slot;
      parent.append(node);
    }
  }

  render() {
    renderShadow(
      this,
      html`
        <layout-grid
          id="main-grid"
          hide-rv
          hide-c
          hide-th
          hide-bh
          scale-v=${this.scale}
          scale-h="1"
        >
          <slot
            id="top"
            name="top"
            slot="tl"
            style="grid-column: auto / span 3"
            @slotchange=${(e) => this.handleSlotChange()}
          >
          </slot>
          <slot
            id="bottom"
            name="bottom"
            slot="bl"
            style="grid-column: auto / span 3"
            @slotchange=${(e) => this.handleSlotChange()}
          >
          </slot>
        </layout-grid>
      `
    );
    this.top = this.shadowRoot.querySelector("#top");
    this.bottom = this.shadowRoot.querySelector("#bottom");
  }
}

customElements.define("layout-split-v", LayoutSplitV);
