import { renderShadow, html, CustomElement } from "../../utils/index.js";
import unique_id from "../../utils/unique_id.js";

class LayoutEmpty extends CustomElement {
  connectedCallback() {
    this.render();
  }

  render() {
    this.id = this.id || unique_id();
    renderShadow(
      this,
      html`
        <style>
          .container {
            position: relative;
            display: flex;
            place-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
            background: var(--bg-dark, #333333);
            color: var(--text-light, #ffffff);
          }
          layout-drop {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
          }
        </style>
        <div class="container">
          Empty
          <layout-drop target=${this.id}></layout-drop>
        </div>
      `
    );
  }
}

customElements.define("layout-empty", LayoutEmpty);
