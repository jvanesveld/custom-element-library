import { renderShadow, html, CustomElement } from "../../utils/index.js";
import unique_id from "../../utils/unique_id.js";

class LayoutPanel extends CustomElement {
  connectedCallback() {
    this.id = this.id || unique_id();
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          fa-icon {
            position: absolute;
            display: grid;
            place-items: center;
            z-index: 1000;
            right: 0;
            top: 0;
            min-width: 1.5rem;
            min-height: 1.5rem;
            color: var(--bg-light, #555555);
            transition: color 0.3s;
            filter: drop-shadow(0 0 0.05em black);
          }
          fa-icon:hover {
            color: var(--text-light, #ffffff);
          }
          .container {
            position: relative;
            width: 100%;
            height: 100%;
            background: var(--bg-dark, #333333);
            color: var(--text-light, #ffffff);
          }
          layout-drop {
            display: var(--drag-area-display, none);
            z-index: 10;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
          }
        </style>
        <div class="container">
          <drag-area panel-id=${this.id}>
            <fa-icon icon="fas fa-arrows-alt"> </fa-icon>
          </drag-area>
          <slot></slot>
          <layout-drop target=${this.id}></layout-drop>
        </div>
      `
    );
  }

  dragStart(e, id) {
    document.documentElement.style.setProperty("--drag-area-display", "block");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "none"
    );
    e.dataTransfer.setData("panel", id);
  }

  dragEnd() {
    document.documentElement.style.setProperty("--drag-area-display", "none");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "block"
    );
  }

  dragEnterBar(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "var(--bg-light, #555555)";
    }
  }

  dragEnter(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "var(--bg-medium, #444444)";
    }
  }

  dragLeave(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
  }

  drop(e) {
    e.preventDefault();
    e.stopPropagation();
    let content_id = e.dataTransfer.getData("panel");
    let element = document.getElementById(content_id);
    this.contentElement.append(element);
    this.current_tab = Infinity;
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
  }

  dropTab(e, drop_id) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
    let drag_id = e.dataTransfer.getData("panel");
    if (drag_id === drop_id) {
      return;
    }
    let drop_index = this.tabs.findIndex(
      (value) => value.content_id === drop_id
    );
    let drag_index = this.tabs.findIndex(
      (value) => value.content_id === drag_id
    );
    let drop_element = document.getElementById(drop_id);
    let drag_element = document.getElementById(drag_id);
    if (drag_index != -1) {
      if (drag_index < drop_index) {
        this.contentElement.insertBefore(
          drag_element,
          drop_element.nextSibling
        );
      }
      if (drop_index < drag_index) {
        this.contentElement.insertBefore(drag_element, drop_element);
      }
    } else if (drag_index < drop_index) {
      this.contentElement.insertBefore(drag_element, drop_element);
    }
    this.current_tab = drop_index;
  }
}

customElements.define("layout-panel", LayoutPanel);
