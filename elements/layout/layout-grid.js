import { renderShadow, html, CustomElement } from "../../utils/index.js";

class LayoutGrid extends CustomElement {
  static get observedAttributes() {
    return [
      "hide_th",
      "hide_bh",
      "hide_lv",
      "hide_rv",
      "hide_c",
      "scale-h",
      "scale-v",
      "snap",
    ];
  }

  snap = 50;
  hide_th;
  hide_bh;
  hide_lv;
  hide_rv;
  hide_c;
  scale_h = "0.5";
  scale_v = "0.5";

  constructor() {
    super();
    this.initInput("hide_th", {
      sync: true,
      attrName: "hide-th",
      isBool: true,
    });
    this.initInput("hide_bh", {
      sync: true,
      attrName: "hide-bh",
      isBool: true,
    });
    this.initInput("hide_lv", {
      sync: true,
      attrName: "hide-lv",
      isBool: true,
    });
    this.initInput("hide_rv", {
      sync: true,
      attrName: "hide-rv",
      isBool: true,
    });
    this.initInput("hide_c", { sync: true, attrName: "hide-c", isBool: true });
    this.initInput("scale_v", { sync: true, attrName: "scale-v" });
    this.initInput("scale_h", { sync: true, attrName: "scale-h" });
    this.initInput("snap", { sync: true });
  }

  connectedCallback() {
    this.render();
  }

  inputChanged() {
    this.render();
  }

  setVertical(pos_y) {
    let d_top = pos_y - this.grid.offsetTop;
    let d_bot = this.grid.offsetHeight - d_top;
    if (this.snap > d_top) {
      this.scale_v = 0;
    } else if (this.snap > d_bot) {
      this.scale_v = 1;
    } else {
      let fraction = Math.min(1, Math.max(0, d_top) / this.grid.offsetHeight);
      this.scale_v = fraction;
    }
  }

  setHorizontal(pos_x) {
    let d_left = pos_x - this.grid.offsetLeft;
    let d_right = this.grid.offsetWidth - d_left;
    if (this.snap > d_left) {
      this.scale_h = 0;
    } else if (this.snap > d_right) {
      this.scale_h = 1;
    } else {
      let fraction = Math.min(1, Math.max(0, d_left) / this.grid.offsetWidth);
      this.scale_h = fraction;
    }
  }

  horizontalDrag(pos_x, _) {
    this.setHorizontal(pos_x);
  }
  verticalDrag(_, pos_y) {
    this.setVertical(pos_y);
  }
  centerDrag(pos_x, pos_y) {
    this.setHorizontal(pos_x);
    this.setVertical(pos_y);
  }

  startDragH(...inputs) {
    this.startDrag(...inputs, "ew-resize");
  }

  startDragC(...inputs) {
    this.startDrag(...inputs, "move");
  }

  startDragV(...inputs) {
    this.startDrag(...inputs, "ns-resize");
  }

  startDrag(e, handleUpdate, cursor) {
    e = e || window.event;
    e.preventDefault();
    handleUpdate(e.clientX, e.clientY);
    document.onmouseup = () => {
      document.onmouseup = null;
      document.onmousemove = null;
      document.body.style.cursor = "";
    };
    document.body.style.cursor = cursor;
    // call a function whenever the cursor moves:
    document.onmousemove = (e) => this.dividerDrag(e, handleUpdate);
  }

  dividerDrag(e, handleUpdate) {
    e = e || window.event;
    e.preventDefault();
    handleUpdate(e.clientX, e.clientY);
    this.render();
  }

  render() {
    let scale_h = parseFloat(this.scale_v);
    let scale_w = parseFloat(this.scale_h);
    renderShadow(
      this,
      html`
        <style>
          #layout-grid {
            display: grid;
            overflow: hidden;
            height: 100%;
            width: 100%;
            grid-template-rows: ${scale_h}fr auto ${1 - scale_h}fr;
            grid-template-columns: ${scale_w}fr auto ${1 - scale_w}fr;
            grid-template-areas:
              "tl th tr"
              "lv c rv"
              "bl bh br";
          }
          slot {
            overflow: hidden;
          }
          .handle {
            z-index:100;
            position: relative;
            height: 100%;
            width: 100%;
            grid-column-start: auto;
            grid-column-end: span 1;
          }
          .t.h {
            ${this.hide_th ? "display: none;" : ""}
            grid-area:th;
          }
          .l.v {
            ${this.hide_lv ? "display: none;" : ""}
            grid-area:lv;
          }
          .r.v {
            ${this.hide_rv ? "display: none;" : ""}
            grid-area:rv;
          }
          .b.h {
            ${this.hide_bh ? "display: none;" : ""}
            grid-area:bh;
          }
          .h {
            cursor: ew-resize;
          }
          .c {
            ${this.hide_c ? "display: none;" : ""}
            cursor: move;
            grid-area:c;
          }
          .v {
            cursor: ns-resize;
          }
          .handle:after {
            position:absolute;
            content: ' ';
          }
          .handle:before {
            position:absolute;
            content: ' ';
            z-index: 1;
          }
          .handle.h:before {
            background: var(--border-color, #444444);
            top: 0;
            bottom: 0;
            left:  calc(-1 * var(--border-width, 1px));
            right: calc(-1 * var(--border-width, 1px));
          }
          .handle.v:before {
            background: var(--border-color, #444444);
            top: calc(-1 * var(--border-width, 1px));
            bottom: calc(-1 * var(--border-width, 1px));
            left: 0;
            right: 0;
          }
          .handle.c:before {
            background: var(--border-color, #444444);
            z-index: 10;
            top: calc(-1 * var(--border-width, 1px));
            bottom: calc(-1 * var(--border-width, 1px));
            left: calc(-1 * var(--border-width, 1px));
            right: calc(-1 * var(--border-width, 1px));
          }
          .handle.h:after {
            top: 0;
            bottom: 0;
            left: calc(-1 * var(--interact-width, 5px));
            right: calc(-1 * var(--interact-width, 5px));
          }
          .handle.v:after {
            top: calc(-1 * var(--interact-width, 5px));
            bottom: calc(-1 * var(--interact-width, 5px));
            left: 0;
            right: 0;
          }
          .handle.c:after {
            z-index: 10;
            top: calc(-1 * var(--interact-width, 5px));
            bottom: calc(-1 * var(--interact-width, 5px));
            left: calc(-1 * var(--interact-width, 5px));
            right: calc(-1 * var(--interact-width, 5px));
          }
          .slot {
            overflow: hidden;
          }
          .slot-container {
            overflow: hidden;
            height: 100%;
            width: 100%;
          }
          #tl {
            grid-area: tl;
          }
          #tr {
            grid-area: tr;
          }
          #bl {
            grid-area: bl;
          }
          #br {
            grid-area: br;
          }
        </style>
        <div id="layout-grid">
          <div id="tl" class="slot"><slot name="tl"></slot></div>
          <div
            @mousedown=${(e) =>
              this.startDragH(e, (...input) => this.horizontalDrag(...input))}
            class="handle h t"
          ></div>
          <div id="tr" class="slot"><slot name="tr"></slot></div>
          <div
            @mousedown=${(e) =>
              this.startDragV(e, (...input) => this.verticalDrag(...input))}
            class="handle v l"
          ></div>
          <div
            @mousedown=${(e) =>
              this.startDragC(e, (...input) => this.centerDrag(...input))}
            class="handle c"
          ></div>
          <div
            @mousedown=${(e) =>
              this.startDragV(e, (...input) => this.verticalDrag(...input))}
            class="handle v r"
          ></div>
          <div id="bl" class="slot"><slot name="bl"></slot></div>
          <div
            @mousedown=${(e) =>
              this.startDragH(e, (...input) => this.horizontalDrag(...input))}
            class="handle h b"
          ></div>
          <div id="br" class="slot"><slot name="br"></slot></div>
        </div>
      `
    );
    this.grid = this.shadowRoot.querySelector("#layout-grid");
  }
}

customElements.define("layout-grid", LayoutGrid);
