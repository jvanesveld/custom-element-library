import unique_id from "../../utils/unique_id.js";

let create_methods = {};
const layout_elements = ["LAYOUT-PANEL", "LAYOUT-TAB"];

let add_methods = {
  "LAYOUT-EMPTY": (first, second) => {
    let parent = first.parentElement;
    first.remove();
    let element = document.createElement("layout-tab");
    element.id = element.id || unique_id();
    element.current = 0;
    element.slot = first.slot;
    first.slot = "";
    element.append(second);
    parent.append(element);
  },
  "LAYOUT-TAB": (first, second) => {
    if (first.contentElement !== second.parentElement) {
      if (!layout_elements.includes(second.tagName)) {
        first.current_tab = Infinity;
        first.contentElement.append(second);
      }
    } else {
      first.current_tab = [].indexOf.call(
        first.contentElement.children,
        second
      );
    }
  },
};

export function createNewArea(element) {
  let tagName = element.tagName;
  if (layout_elements.includes(tagName)) {
    return element;
  }
  if (create_methods[tagName]) {
    create_methods[tagName](element);
    return;
  }
  const ignore = ["LAYOUT-PANEL"];
  let tabs = document.createElement("layout-tab");
  tabs.append(element);
  return tabs;
}

export function addToArea(first, second) {
  let tagName = first.tagName;
  if (!add_methods[tagName]) {
    console.error(`No add method defined for ${tagName}`);
    return;
  }
  add_methods[tagName](first, second);
}

export function canAdd(id) {
  const element = document.getElementById(id);
  let tagName = element.tagName;
  return add_methods[tagName];
}
