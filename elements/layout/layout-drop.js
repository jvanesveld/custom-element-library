import { renderShadow, html, CustomElement } from "../../utils/index.js";
import { addToArea, canAdd, createNewArea } from "./handle-drops.js";

class LayoutDrop extends CustomElement {
  static get observedAttributes() {
    return ["target"];
  }

  target = "";
  drag_counter = 0;
  highlight_areas = {
    top: {
      top: 0,
      bottom: "50%",
      left: 0,
      right: 0,
    },
    bottom: {
      top: "50%",
      bottom: 0,
      left: 0,
      right: 0,
    },
    left: {
      top: 0,
      bottom: 0,
      left: 0,
      right: "50%",
    },
    right: {
      top: 0,
      bottom: 0,
      left: "50%",
      right: 0,
    },
    center: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  };

  handle_drop_areas = {
    top: (first, second) => {
      let parent = first.parentElement;
      let element = document.createElement("layout-split-v");
      element.slot = first.slot;
      first.slot = "bottom";
      let area = createNewArea(second);
      area.slot = "top";
      element.append(first, area);
      parent.append(element);
    },
    bottom: (first, second) => {
      let parent = first.parentElement;
      let element = document.createElement("layout-split-v");
      element.slot = first.slot;
      first.slot = "top";
      let area = createNewArea(second);
      area.slot = "bottom";
      element.append(first, area);
      parent.append(element);
    },
    left: (first, second) => {
      let parent = first.parentElement;
      let element = document.createElement("layout-split-h");
      element.slot = first.slot;
      first.slot = "right";
      let area = createNewArea(second);
      area.slot = "left";
      element.append(first, area);
      parent.append(element);
    },
    right: (first, second) => {
      let parent = first.parentElement;
      let element = document.createElement("layout-split-h");
      element.slot = first.slot;
      first.slot = "left";
      let area = createNewArea(second);
      area.slot = "right";
      element.append(first, area);
      parent.append(element);
    },
    center: (first, second) => {
      addToArea(first, second);
    },
  };

  constructor() {
    super();
    this.initInput("target", { sync: true });
  }

  connectedCallback() {
    this.render();
  }

  inputChanged() {
    this.render();
  }

  dragEnter(e, area) {
    this.drag_counter++;
    e.preventDefault();
    e.stopPropagation();
    if (this.drag_counter >= 1) {
      this.highlight.style.opacity = 0.5;
    }
    Object.assign(this.highlight.style, this.highlight_areas[area]);
  }

  dragLeave(e) {
    this.drag_counter--;
    e.preventDefault();
    e.stopPropagation();
    if (this.drag_counter === 0) {
      this.highlight.style.opacity = "";
    }
  }

  dragDrop(e, area) {
    this.drag_counter = 0;
    e.preventDefault();
    e.stopPropagation();
    this.highlight.style.opacity = "";
    let content_id = e.dataTransfer.getData("panel");
    let first = document.getElementById(this.target);
    let second = document.getElementById(content_id);
    this.handle_drop_areas[area](first, second);
  }

  dragOver(e) {
    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    let gridAreas = canAdd(this.target)
      ? `"l t r"
      "l c r"
      "l b r"`
      : `"l t r"
      "l t r"
      "l t r"
      "l b r"
      "l b r"
      "l b r"`;
    renderShadow(
      this,
      html`
        <style>
          .container {
            display: grid;
            width: 100%;
            height: 100%;
            grid-template-areas: ${gridAreas};
          }
          .area {
            width: 100%;
            height: 100%;
          }
          .l {
            grid-area: l;
          }
          .t {
            grid-area: t;
          }
          .c {
            grid-area: c;
          }
          .b {
            grid-area: b;
          }
          .r {
            grid-area: r;
          }
          #highlight {
            pointer-events: none;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            background: var(--bg-light, #555555);
            opacity: 0;
            transition: all 0.2s;
          }
        </style>
        <div class="container">
          <div id="highlight"></div>
          <div
            class="l area"
            @dragover=${(e) => this.dragOver(e)}
            @dragenter=${(e) => this.dragEnter(e, "left")}
            @dragleave=${(e) => this.dragLeave(e)}
            @drop=${(e) => this.dragDrop(e, "left")}
          ></div>
          <div
            class="t area"
            @dragover=${(e) => this.dragOver(e)}
            @dragenter=${(e) => this.dragEnter(e, "top")}
            @dragleave=${(e) => this.dragLeave(e)}
            @drop=${(e) => this.dragDrop(e, "top")}
          ></div>

          ${canAdd(this.target)
            ? html` <div
                class="c area"
                @dragover=${(e) => this.dragOver(e)}
                @dragenter=${(e) => this.dragEnter(e, "center")}
                @dragleave=${(e) => this.dragLeave(e)}
                @drop=${(e) => this.dragDrop(e, "center")}
              ></div>`
            : ""}

          <div
            class="b area"
            @dragover=${(e) => this.dragOver(e)}
            @dragenter=${(e) => this.dragEnter(e, "bottom")}
            @dragleave=${(e) => this.dragLeave(e)}
            @drop=${(e) => this.dragDrop(e, "bottom")}
          ></div>
          <div
            class="r area"
            @dragover=${(e) => this.dragOver(e)}
            @dragenter=${(e) => this.dragEnter(e, "right")}
            @dragleave=${(e) => this.dragLeave(e)}
            @drop=${(e) => this.dragDrop(e, "right")}
          ></div>
        </div>
      `
    );
    this.highlight = this.shadowRoot.querySelector("#highlight");
    this.container = this.shadowRoot.querySelector(".container");
  }
}

customElements.define("layout-drop", LayoutDrop);
