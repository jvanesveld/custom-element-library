import { renderShadow, html, CustomElement } from "../../utils/index.js";

class DragArea extends CustomElement {
  static get observedAttributes() {
    return ["panel-id"];
  }

  panelId = "";

  constructor() {
    super();
    this.initInput("panelId", { sync: true, attrName: "panel-id" });
  }

  connectedCallback() {
    this.render();
  }

  handleSlotChange() {
    for (const element of this.contentSlot.assignedNodes()) {
      if (element instanceof HTMLElement) {
        if (!element.draggable) {
          element.setAttribute("draggable", "true");
          element.addEventListener("dragstart", (e) => this.dragStart(e));
          element.addEventListener("dragend", (e) => this.dragEnd(e));
        }
      }
    }
  }

  render() {
    renderShadow(
      this,
      html` <slot @slotchange=${(e) => this.handleSlotChange()}> </slot> `
    );
    this.contentSlot = this.shadowRoot.querySelector("slot");
    this.handleSlotChange();
  }

  dragStart(e) {
    document.documentElement.style.setProperty("--drag-area-display", "block");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "none"
    );
    e.dataTransfer.setData("panel", this.panelId);
    console.log(e.dataTransfer.getData("panel"));
  }

  dragEnd() {
    document.documentElement.style.setProperty("--drag-area-display", "none");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "block"
    );
  }
}

customElements.define("drag-area", DragArea);
