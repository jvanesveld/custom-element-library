import { renderShadow, html, CustomElement } from "../../utils/index.js";

class LayoutSplitH extends CustomElement {
  static get observedAttributes() {
    return ["scale"];
  }

  scale = "0.5";

  constructor() {
    super();
    this.initInput("scale", { sync: true, attrName: "scale" });
  }

  connectedCallback() {
    this.render();
  }

  inputChanged() {
    this.render();
  }

  handleSlotChange() {
    let parent = this.parentElement;
    let slot = this.slot;
    if (!parent) {
      return;
    }
    let left =
      this.left.assignedNodes()[0] === undefined ||
      this.left.assignedNodes()[0].tagName === "LAYOUT-EMPTY";
    let right =
      this.right.assignedNodes()[0] === undefined ||
      this.right.assignedNodes()[0].tagName === "LAYOUT-EMPTY";
    if (left && right) {
      this.remove();
      parent.append(document.createElement("layout-empty"));
    } else if (left) {
      this.remove();
      let node = this.right.assignedNodes()[0];
      node.slot = slot;
      parent.append(node);
    } else if (right) {
      this.remove();
      let node = this.left.assignedNodes()[0];
      node.slot = slot;
      parent.append(node);
    }
  }

  render() {
    renderShadow(
      this,
      html`
        <layout-grid
          id="main-grid"
          hide-c
          hide-lv
          hide-rv
          hide-bh
          scale-h=${this.scale}
          scale-v="1"
        >
          <slot
            id="left"
            name="left"
            slot="tl"
            style="grid-row: auto / span 3"
            @slotchange=${(e) => this.handleSlotChange()}
          >
          </slot>
          <slot
            id="right"
            name="right"
            slot="tr"
            style="grid-row: auto / span 3"
            @slotchange=${(e) => this.handleSlotChange()}
          >
          </slot>
        </layout-grid>
      `
    );
    this.left = this.shadowRoot.querySelector("#left");
    this.right = this.shadowRoot.querySelector("#right");
  }
}

customElements.define("layout-split-h", LayoutSplitH);
