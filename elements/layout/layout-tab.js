import { CustomElement, html, render } from "../../utils/index.js";
import unique_id from "../../utils/unique_id.js";
import("../fa-icon/index.js");

class LayoutTab extends CustomElement {
  static get observedAttributes() {
    return ["current-tab"];
  }

  current_tab = 0;
  tabs = [];

  constructor() {
    super();
    this.collapsible = false;
    this.initInput("current_tab", { sync: true, attrName: "current-tab" });
    this.initInput("tabs", { sync: false });
    this.mutObserver = new MutationObserver((_) => {
      if (this.contentElement.children.length == 0) {
        let parent = this.parentElement;
        let empty = document.createElement("layout-empty");
        empty.slot = this.slot;
        this.remove();
        parent.append(empty);
      }
      this.generateTabs(this.contentElement.children);
      this.limitCurrentTab();
    });
  }

  append(element) {
    if (this.contentElement) {
      element.id = element.id || unique_id();
      this.contentElement.append(element);
    } else {
      super.append(element);
    }
  }

  connectedCallback() {
    this.id = this.id || unique_id();
    if (!this.contentElement) {
      let children = this.removeChildren(this);
      this.generateTabs(children);
      this.render();
      children.forEach((child) => this.contentElement.append(child));
    } else {
      this.render();
    }
    this.mutObserver.observe(this.contentElement, { childList: true });
  }

  generateTabs(children) {
    let tabs = [];
    for (let child of children) {
      tabs.push({
        title: child.getAttribute("panel-title") || "unknown",
        content_id: child.id,
      });
    }
    this.tabs = tabs;
  }

  removeChildren(target) {
    let children = [];
    while (target.lastChild) {
      target.lastChild.id = target.lastChild.id || unique_id();
      if (target.lastChild instanceof HTMLElement) {
        children.unshift(this.lastChild);
      }
      target.lastChild.remove();
    }
    return children;
  }

  inputChanged() {
    this.render();
  }

  render() {
    let child_index = parseInt(this.current_tab) + 1;
    render(
      this,
      html`
        <style>
          .tab-container {
            position: relative;
            display: grid;
            height: 100%;
            width: 100%;
            grid-template-rows: auto 1fr;
            grid-template-columns: 1fr min-content;
            grid-template-areas:
              "titles controls"
              "content content";
          }
          .drag-icon {
            position: absolute;
            display: grid;
            place-items: center;
            z-index: 1000;
            right: 0;
            top: 0;
            min-width: 2rem;
            min-height: 2rem;
            color: var(--bg-light, #555555);
            transition: color 0.3s;
            filter: drop-shadow(0 0 0.05em black);
          }
          .drag-icon:hover {
            color: var(--text-light, #ffffff);
          }
          .tab-content {
            height: 100%;
            width: 100%;
            background: var(--bg-medium, #444444);
            color: var(--text-light, #ffffff);
          }
          .tab-titles {
            display: grid;
            overflow: auto;
            grid-auto-flow: column;
            justify-content: left;
            min-height: 2rem;
            grid-area: titles;
            background: var(--bg-medium, #444444);
            color: var(--text-light, #ffffff);
            font-family: var(--text-font, "Arial, sans-serif");
          }
          .tab-title:not(:first-child) {
            border-left: 1px solid var(--bg-medium, #444444);
          }
          .tab-title {
            font-size: 1.5rem;
            position: relative;
            display: flex;
            place-items: center;
            justify-content: center;
            cursor: pointer;
            background: var(--bg-light, #555555);
            color: var(--text-medium, #dddddd);
            min-width: 5em;
            max-width: 5em;
            padding-left: 0.3rem;
            padding-right: 0.5rem;
            padding-top: 0.3rem;
            padding-bottom: 0.3rem;
          }
          .tab-title fa-icon {
            position: absolute;
            right: 0.3em;
            opacity: 0;
            font-size: 0.8rem;
            padding-left: 0.5rem;
            transition: opacity 0.08s ease-in;
          }
          .tab-title:hover fa-icon {
            opacity: 1;
          }
          .tab-title-content {
            font-size: 1rem;
            display: flex;
            place-items: center;
            pointer-events: none;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
          }
          .tab-title-content > * {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
          }
          .tab-content-container {
            overflow: auto;
            grid-area: content;
            position: relative;
            height: 100%;
            width: 100%;
          }
          #${this.id}
            > .tab-container
            > .tab-titles
            > .tab-title:nth-child(${child_index}) {
            background: var(--bg-dark, #333333);
            color: var(--text-light, #ffffff);
          }
          #${this.id}
            > .tab-container
            > .tab-content-container
            > .tab-content
            > *:not(:nth-child(${child_index})) {
            display: none;
          }
          layout-drop {
            display: var(--drag-area-display, none);
            z-index: 10;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
          }
        </style>
        <div class="tab-container">
          <div
            class="tab-titles"
            @drop=${(e) => this.drop(e)}
            @dragover=${(e) => e.preventDefault()}
            @dragenter=${(e) => this.dragEnterBar(e)}
            @dragleave=${(e) => this.dragLeave(e)}
          >
            ${Object.values(this.tabs).map((title, index) =>
              this.titleTemplate(title, index)
            )}
          </div>
          <div class="tab-controls">
            <drag-area panel-id=${this.id}>
              <fa-icon class="drag-icon" icon="fas fa-arrows-alt"> </fa-icon>
            </drag-area>
          </div>
          <div class="tab-content-container">
            <layout-drop target=${this.id}></layout-drop>
            <div class="tab-content"></div>
          </div>
        </div>
      `
    );
    this.contentElement = this.querySelector(".tab-content");
  }

  dragStart(e, tab) {
    document.documentElement.style.setProperty("--drag-area-display", "block");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "none"
    );
    e.dataTransfer.setData("panel", tab);
  }

  dragEnd() {
    document.documentElement.style.setProperty("--drag-area-display", "none");
    document.documentElement.style.setProperty(
      "--drag-area-display-inverse",
      "block"
    );
  }

  dragEnterBar(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "var(--bg-light, #555555)";
    }
  }

  dragEnter(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "var(--bg-medium, #444444)";
    }
  }

  dragLeave(e) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
  }

  drop(e) {
    e.preventDefault();
    e.stopPropagation();
    let content_id = e.dataTransfer.getData("panel");
    let element = document.getElementById(content_id);
    this.contentElement.append(element);
    this.current_tab = Infinity;
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
  }

  dropTab(e, drop_id) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target instanceof HTMLElement) {
      e.target.style.background = "";
    }
    let drag_id = e.dataTransfer.getData("panel");
    if (drag_id === drop_id) {
      return;
    }
    let drop_index = this.tabs.findIndex(
      (value) => value.content_id === drop_id
    );
    let drag_index = this.tabs.findIndex(
      (value) => value.content_id === drag_id
    );
    let drop_element = document.getElementById(drop_id);
    let drag_element = document.getElementById(drag_id);
    if (drag_index != -1) {
      if (drag_index < drop_index) {
        this.contentElement.insertBefore(
          drag_element,
          drop_element.nextSibling
        );
      }
      if (drop_index < drag_index) {
        this.contentElement.insertBefore(drag_element, drop_element);
      }
    } else if (drag_index < drop_index) {
      this.contentElement.insertBefore(drag_element, drop_element);
    }
    this.current_tab = drop_index;
  }

  removeContent(index) {
    this.contentElement.children.item(index).remove();
  }

  limitCurrentTab() {
    this.current_tab = Math.min(this.current_tab, this.tabs.length - 1);
  }

  titleTemplate(tab, index) {
    return html` <div
      class="tab-title"
      draggable="true"
      @click=${() => (this.current_tab = index)}
      @dragenter=${(e) => this.dragEnter(e)}
      @dragleave=${(e) => this.dragLeave(e)}
      @dragstart=${(e) => this.dragStart(e, tab.content_id)}
      @dragend=${(e) => this.dragEnd()}
      @drop=${(e) => this.dropTab(e, tab.content_id)}
    >
      <div class="tab-title-content">
        <span>${tab.title}</span>
      </div>
      <fa-icon
        style="display: var(--drag-area-display-inverse, block)"
        icon="fas fa-times"
        color="white"
        @click=${(e) => {
          e.preventDefault();
          e.stopPropagation();
          this.removeContent(index);
        }}
      >
      </fa-icon>
    </div>`;
  }
}

customElements.define("layout-tab", LayoutTab);
