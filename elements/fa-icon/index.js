import { renderShadow, html, CustomElement } from "../../utils/index.js";

const dir = import.meta.url.substring(0, import.meta.url.lastIndexOf("/"));

const filename = `${dir}/fa/css/all.css`;
const style = document.createElement("link");
style.setAttribute("rel", "stylesheet");
style.setAttribute("href", filename);
document.getElementsByTagName("head")[0].appendChild(style);

/**
 * MUST INCLUDE CSS IN HEADER:
 * <link rel="stylesheet" type="text/css" href="PATH/fa/css/all.css">
 */
class FAIcon extends CustomElement {
  static get observedAttributes() {
    return ["icon", "color"];
  }

  constructor() {
    super();
    this.initInput("icon", { sync: true });
    this.initInput("color", { sync: true });
  }

  connectedCallback() {
    this.render();
  }

  inputChanged() {
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
        <link rel="stylesheet" type="text/css" href="${filename}" />
        <i class=${this.icon} style="color: ${this.color}"></i>
      `
    );
  }
}

customElements.define("fa-icon", FAIcon);
