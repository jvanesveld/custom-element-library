import { html, CustomElement, renderShadow } from "../../utils/index.js";

class ItemRow extends CustomElement {
  static get observedAttributes() {
    return ["name", "icon", "icon-color"];
  }

  depth = 0;
  name = "unknown";
  icon = "fas fa-question-circle";
  icon_color = "white";
  context_options = undefined;

  connectedCallback() {
    this.initInput("name", { sync: true });
    this.initInput("icon", { sync: true });
    this.initInput("icon_color", { sync: true, attrName: "icon-color" });
    this.initInput("depth", { sync: false });
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          label {
            user-select: none;
            cursor: pointer;
            display: grid;
            grid-auto-flow: column;
            align-items: center;
            justify-content: left;
            grid-gap: 10px;
            width: 100%;
            color: var(--text-light, #ffffff);
            padding-left: calc(${this.depth} * var(--tree-indent, 10px) + 10px);
            padding-top: 0.1em;
            padding-bottom: 0.1em;
          }

          fa-icon {
            font-size: 0.9em;
            color: ${this.icon_color};
          }

          label:hover {
            background: var(--bg-light, #555555);
          }
        </style>
        <label><fa-icon icon=${this.icon}></fa-icon> ${this.name}</label>
      `
    );
  }
}

customElements.define("item-row", ItemRow);
