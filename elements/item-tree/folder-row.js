import { html, CustomElement, renderShadow } from "../../utils/index.js";

class FolderRow extends CustomElement {
  static get observedAttributes() {
    return ["name", "icon"];
  }
  icon = "fas fa-chevron-right";
  depth = 0;
  name = "unknown";

  connectedCallback() {
    this.initInput("name", { sync: true });
    this.initInput("icon", { sync: true });
    this.initInput("depth", { sync: false });
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
      <style>
        /* Hide checkbox */
        input[type=checkbox] { display: none; }

        /* Toggle slots */
        slot { display: none; }
        input:checked ~ slot { display: block; }

        label {
          user-select: none;
          cursor: pointer;
          display: grid;
          grid-auto-flow: column;
          align-items: center;
          justify-content: left;
          grid-gap: 10px;
          min-height: 1.5rem;
          width: 100%;
          color: var(--text-light, #ffffff);
          padding-left: calc(${this.depth}*var(--tree-indent, 10px) + 10px);
          padding-top: 0.1em;
          padding-bottom: 0.1em;
        }

        fa-icon {
          width: 0.9em;
          font-size: 0.9em;
        }

        label:hover {
          background: var(--bg-light, #555555);
        }
      </style>

      </div>
      <input type="checkbox" id="toggle" @change=${(e) => {
        this.icon = this.checkbox.checked
          ? "fas fa-chevron-down"
          : "fas fa-chevron-right";
        this.render();
      }}/>
      <label for="toggle">
        ${this.icon != "" ? html`<fa-icon icon=${this.icon}></fa-icon>` : ``}
        ${this.name}
      </label>
      <slot></slot>
    `
    );
    this.checkbox = this.shadowRoot.querySelector("#toggle");
  }
}

customElements.define("folder-row", FolderRow);
