import { renderShadow, html, CustomElement } from "../../utils/index.js";
import "./item-row.js";
import "./folder-row.js";
import "../context-menu/index.js";

class ItemTree extends CustomElement {
  icons = {
    js: "fab fa-js",
  };
  colors = {
    js: "yellow",
  };
  items = [
    {
      name: "Folder 1",
      folder: true,
      options: [
        { name: "Try me!", action: console.log },
        { separator: true },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
      ],
      items: [
        {
          name: "Sub folder",
          folder: true,
          items: [
            {
              name: "item 2",
              type: "js",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
        {
          name: "item 1",
          type: "js",
          options: [{ name: "Try me!", action: console.log }],
        },
      ],
    },
  ];

  constructor() {
    super();
    this.initInput("items");
    this.initInput("icons");
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          #container {
            position: relative;
            width: 100%;
            height: 100%;
            background: var(--bg-dark, #333333);
          }
        </style>
        <div id="container">
          ${this.items.map((item) => this.renderRow(item))}
        </div>
        <context-menu></context-menu>
      `
    );
    this.context_menu = this.shadowRoot.querySelector("context-menu");
  }

  showContextMenu(e, item) {
    e.preventDefault();
    e.stopPropagation();
    const bounding = this.getBoundingClientRect();
    this.context_menu.show = true;
    this.context_menu.x_pos = `${e.clientX - bounding.left}px`;
    this.context_menu.y_pos = `${e.clientY - bounding.top}px`;
    this.context_menu.options = item.options;
  }

  renderRow(item, depth = 0) {
    if (item.folder) {
      return html`
        <folder-row
          name=${item.name}
          .depth=${depth}
          @contextmenu=${(e) => this.showContextMenu(e, item)}
        >
          ${item.items.map((item) => this.renderRow(item, depth + 1))}
        </folder-row>
      `;
    } else {
      return html`
        <item-row
          name=${item.name}
          .depth=${depth}
          icon=${this.icons[item.type]}
          icon-color=${this.colors[item.type]}
          @contextmenu=${(e) => this.showContextMenu(e, item)}
        >
        </item-row>
      `;
    }
  }
}

customElements.define("item-tree", ItemTree);
