import { html, CustomElement, renderShadow } from "../../utils/index.js";

class Toolbar extends CustomElement {
  options = [
    {
      name: "File",
      options: [
        { name: "Try me!", action: console.log },
        { separator: true },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
      ],
    },
    {
      name: "Edit",
      options: [
        { name: "Try me!", action: console.log },
        { separator: true },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
        {
          name: "Sub options",
          options: [
            { name: "Try me!", action: console.log },
            {
              name: "Sub options",
              options: [{ name: "Try me!", action: console.log }],
            },
          ],
        },
      ],
    },
  ];
  current = [];
  context_menu = undefined;
  show = false;

  constructor() {
    super();
    this.listenCloseEvents();
  }

  connectedCallback() {
    this.render();
  }

  render() {
    renderShadow(
      this,
      html`
        <style>
          .container {
            width: 100%;
            min-height: 2rem;
            background: var(--bg-medium, #444444);
            color: white;
            display: grid;
            grid-auto-flow: column;
            justify-content: left;
            align-items: center;
          }
          span {
            display: flex;
            align-items: center;
            position: relative;
            padding-right: 1rem;
            padding-left: 1rem;
            height: 2rem;
            cursor: pointer;
          }
          span:hover {
            background: var(--bg-dark, #333333);
          }
        </style>
        <div class="container">
          ${this.options.map((option) => this.renderOption(option))}
        </div>
      `
    );
    this.context_menu = this.shadowRoot.querySelector("context-menu");
  }

  renderOption(option) {
    return html`
      <span
        class="option"
        @click=${(e) => {
          const menu = e.target.querySelector("context-menu");
          menu.options = option.options;
          this.show = !this.show;
          menu.show = !menu.show;
        }}
        @mouseleave=${(e) => {
          const menu = e.target.querySelector("context-menu");
          menu.show = false;
        }}
        @mouseenter=${(e) => {
          if (this.show) {
            const menu = e.target.querySelector("context-menu");
            menu.options = option.options;
            menu.show = true;
          }
        }}
      >
        ${option.name}
        <context-menu y-pos="2rem"></context-menu>
      </span>
    `;
  }

  listenCloseEvents() {
    window.addEventListener("blur", () => {
      this.show = false;
    });
    document.addEventListener("mousedown", () => {
      this.show = false;
    });
    document.addEventListener("mousewheel", () => {
      this.show = false;
    });
    document.addEventListener("keydown", (e) => {
      if (e.key === "Escape") {
        this.show = false;
      }
    });
    this.addEventListener("mousedown", (e) => {
      e.stopPropagation();
    });
  }

  showContextMenu(e, options) {
    e.preventDefault();
    e.stopPropagation();
    this.context_menu.show = true;
    this.context_menu.x_pos = e.clientX;
    this.context_menu.y_pos = e.clientY;
    this.context_menu.options = options;
  }
}

customElements.define("tool-bar", Toolbar);
