import { html, CustomElement, render } from "../../utils/index.js";

class AppRouter extends CustomElement {
  constructor() {
    super();
    this.initInput("routes");
    this.resolveRoute = () => {
      return {
        template: html``,
        afterRender: () => {
          return;
        },
      };
    };
    window.addEventListener("popstate", () => {
      this.render();
    });
    this.render();
  }

  connectedCallback() {
    this.refreshResolver();
    this.render();
  }

  inputChanged() {
    this.refreshResolver();
    this.render();
  }

  refreshResolver() {
    if (this.routes) {
      this.routeMatches = this.routes.map((route) => {
        const matches = route.path.matchAll(
          /(?:([^:*]*)*((?:\/?:[^/*]*)*)(\*+[^:/]*)*)/g
        );
        return [...matches];
      });
      this.resolveRoute = async (path) => {
        let i = 0;
        while (i < this.routeMatches.length) {
          let tmpPath = path;
          const params = {};
          const matches = this.routeMatches[i];
          let pathmatches = true;
          for (let match of matches) {
            if (match[0] === "" && match[1] === undefined && match[2] === "") {
              pathmatches = tmpPath === "";
              break;
            }
            if (!tmpPath.startsWith(match[1])) {
              pathmatches = false;
              break;
            }
            tmpPath = tmpPath.replace(match[1], "");
            if (match[2]) {
              const paramNames = match[2].replaceAll(":", "").split("/");
              for (let paramName of paramNames) {
                const paramValue = tmpPath.split("/")[0];
                if (paramValue == "") {
                  pathmatches = false;
                  break;
                }
                params[paramName] = paramValue;
                if (tmpPath.includes("/")) {
                  tmpPath = tmpPath.substring(tmpPath.indexOf("/") + 1);
                } else {
                  tmpPath = "";
                }
              }
            }
          }
          if (pathmatches) {
            return {
              index: i,
              template: await this.routes[i].template(params),
              afterRender: () => {
                if (this.routes[i].afterRender) {
                  this.routes[i].afterRender();
                }
              },
            };
          }
          i++;
        }
        return {
          template: html``,
          afterRender: () => {
            return;
          },
        };
      };
    }
  }

  getCurrentPath() {
    return window.location.hash.replace("#", "");
  }

  async render() {
    const { template, afterRender } = await this.resolveRoute(
      this.getCurrentPath()
    );
    render(this, template);
    afterRender();
  }
}

customElements.define("app-router", AppRouter);
