# Custom Elements Library

Contains:

- fa-icon: Custom element wrapper for free Font-Awesome icons
- layout: Multiple elements used to reproduce vs code like window management
- text-editor: Basic custom element wrapper for code-mirror
